package smartbox.com.demo.di.modules;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import smartbox.com.demo.data.preferences.AppPreferences;
import smartbox.com.demo.presentation.view.util.Util;

@Module
public class AppModule {

    Application application;

    public AppModule(Application application){
        this.application=application;
    }


    @Provides
    public Application provideApplication(){
        return application;
    }


    @Provides
    public Context provideContextApplication(){
        return application.getApplicationContext();
    }


    @Provides
    public AppPreferences provideAppPreferences(Context context){
        return new AppPreferences(context);
    }

}
