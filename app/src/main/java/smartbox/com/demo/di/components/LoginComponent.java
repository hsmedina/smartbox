package smartbox.com.demo.di.components;

import dagger.Component;
import smartbox.com.demo.di.modules.AppModule;
import smartbox.com.demo.di.modules.LoginModule;
import smartbox.com.demo.di.modules.NetModule;
import smartbox.com.demo.presentation.view.activitys.LoginActivity;

@Component(modules = LoginModule.class,dependencies = AppComponent.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
