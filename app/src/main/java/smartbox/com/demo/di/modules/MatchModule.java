package smartbox.com.demo.di.modules;

import dagger.Module;
import dagger.Provides;
import smartbox.com.demo.data.network.ApiNetwork;
import smartbox.com.demo.data.repository.DataSource.MatchDataSource;
import smartbox.com.demo.data.repository.MatchRepository;
import smartbox.com.demo.domain.repository.DataRepository;

@Module
public class MatchModule {

    @Provides
    MatchDataSource provideMatchDataSource(ApiNetwork apiNetwork) {
        return new MatchDataSource(apiNetwork);
    }

    @Provides
    DataRepository provideDataRepository(MatchRepository matchRepository){
        return matchRepository;
    }
}
