package smartbox.com.demo.di.components;

import android.content.Context;

import dagger.Component;
import smartbox.com.demo.data.network.ApiNetwork;
import smartbox.com.demo.di.modules.AppModule;
import smartbox.com.demo.di.modules.NetModule;

@Component(modules = {AppModule.class,NetModule.class})
public interface AppComponent {
    Context context();
    ApiNetwork apiNetwork();
}
