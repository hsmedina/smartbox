package smartbox.com.demo.di.components;

import dagger.Component;
import smartbox.com.demo.di.modules.MatchModule;
import smartbox.com.demo.presentation.view.activitys.MatchsActivity;

@Component(modules = MatchModule.class,dependencies = AppComponent.class)
public interface MatchComponent {
    void inject(MatchsActivity matchsActivity);
}
