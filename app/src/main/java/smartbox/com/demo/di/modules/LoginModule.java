package smartbox.com.demo.di.modules;

import dagger.Module;
import dagger.Provides;
import smartbox.com.demo.data.network.ApiNetwork;
import smartbox.com.demo.data.repository.AuthRepository;
import smartbox.com.demo.data.repository.DataSource.AuthDataSource;
import smartbox.com.demo.domain.repository.LoginRepository;

@Module
public class LoginModule {

    @Provides
    AuthDataSource provideUserDataSource(ApiNetwork apiNetwork) {
        return new AuthDataSource(apiNetwork);
    }

    @Provides
    LoginRepository provideLoginRepository(AuthRepository authRepository){
        return authRepository;
    }
}
