package smartbox.com.demo.data.repository;

import javax.inject.Inject;

import io.reactivex.Observable;
import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.data.repository.DataSource.AuthDataSource;
import smartbox.com.demo.domain.repository.LoginRepository;
import smartbox.com.demo.presentation.model.BodyAuthModel;

public class AuthRepository implements LoginRepository {


    AuthDataSource datasource;


    @Inject
    public AuthRepository(AuthDataSource datasource){
        this.datasource=datasource;
    }

    @Override
    public Observable<ResponseAuth> login(BodyAuthModel body) {
        return datasource.getToeknAuth(body);
    }

}
