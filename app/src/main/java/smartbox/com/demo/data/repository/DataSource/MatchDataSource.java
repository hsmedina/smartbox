package smartbox.com.demo.data.repository.DataSource;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartbox.com.demo.data.network.ApiNetwork;
import smartbox.com.demo.data.network.model.ResponseMatch;

public class MatchDataSource {

    final ApiNetwork network;

    @Inject
    public MatchDataSource(ApiNetwork network){
        this.network=network;
    }

    public Observable<ResponseMatch> getListMatch(String token) {
        return getObservable(network.getListMatch("Bearer " + token));
    }

    @NonNull
    public static Observable<ResponseMatch> getObservable(@NonNull final Call<ResponseMatch> responseAuth) {

        return Observable.create(new ObservableOnSubscribe<ResponseMatch>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseMatch> emitter) throws Exception {

                responseAuth.enqueue(new Callback<ResponseMatch>() {
                    @Override
                    public void onResponse(Call<ResponseMatch> call, Response<ResponseMatch> response) {

                        if(!emitter.isDisposed()){
                            emitter.onNext(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseMatch> call, Throwable t) {

                        if(!emitter.isDisposed())
                            emitter.onError(t);
                    }
                });

            }
        });


    }
}
