package smartbox.com.demo.data.repository;

import javax.inject.Inject;

import io.reactivex.Observable;
import smartbox.com.demo.data.network.model.ResponseMatch;
import smartbox.com.demo.data.repository.DataSource.MatchDataSource;
import smartbox.com.demo.domain.repository.DataRepository;

public class MatchRepository implements DataRepository {

    MatchDataSource datasource;

    @Inject
    public MatchRepository(MatchDataSource datasource){
        this.datasource=datasource;
    }

    @Override
    public Observable<ResponseMatch> getList(String token) {
        return datasource.getListMatch(token);
    }
}
