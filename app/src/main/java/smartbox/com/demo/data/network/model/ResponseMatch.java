package smartbox.com.demo.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMatch {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("sections")
        @Expose
        private List<Section> sections = null;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;

        public List<Section> getSections() {
            return sections;
        }

        public void setSections(List<Section> sections) {
            this.sections = sections;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

    }


    public class AwayTeam {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("shortName")
        @Expose
        private String shortName;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortName() {
            return shortName;
        }

        public void setShortName(String shortName) {
            this.shortName = shortName;
        }

    }

    public class HomeTeam {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("shortName")
        @Expose
        private String shortName;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortName() {
            return shortName;
        }

        public void setShortName(String shortName) {
            this.shortName = shortName;
        }

    }

    public class Item {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("statusCategory")
        @Expose
        private String statusCategory;
        @SerializedName("startDate")
        @Expose
        private String startDate;
        @SerializedName("homeTeam")
        @Expose
        private HomeTeam homeTeam;
        @SerializedName("awayTeam")
        @Expose
        private AwayTeam awayTeam;
        @SerializedName("homeScore")
        @Expose
        private Integer homeScore;

        @SerializedName("awayScore")
        @Expose
        private Integer awayScore;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatusCategory() {
            return statusCategory;
        }

        public void setStatusCategory(String statusCategory) {
            this.statusCategory = statusCategory;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public HomeTeam getHomeTeam() {
            return homeTeam;
        }

        public void setHomeTeam(HomeTeam homeTeam) {
            this.homeTeam = homeTeam;
        }

        public AwayTeam getAwayTeam() {
            return awayTeam;
        }

        public void setAwayTeam(AwayTeam awayTeam) {
            this.awayTeam = awayTeam;
        }

        public Integer getHomeScore() {
            return homeScore;
        }

        public void setHomeScore(Integer homeScore) {
            this.homeScore = homeScore;
        }


        public Integer getAwayScore() {
            return awayScore;
        }

        public void setAwayScore(Integer awayScore) {
            this.awayScore = awayScore;
        }
    }


    public class Section {

        @SerializedName("index")
        @Expose
        private Integer index;
        @SerializedName("title")
        @Expose
        private Title title;

        public Integer getIndex() {
            return index;
        }

        public void setIndex(Integer index) {
            this.index = index;
        }

        public Title getTitle() {
            return title;
        }

        public void setTitle(Title title) {
            this.title = title;
        }

    }

    public class Title {

        @SerializedName("original")
        @Expose
        private String original;

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

    }
}
