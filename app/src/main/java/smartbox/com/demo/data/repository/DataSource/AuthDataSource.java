package smartbox.com.demo.data.repository.DataSource;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartbox.com.demo.data.network.ApiNetwork;
import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.presentation.model.BodyAuthModel;

public class AuthDataSource {


    final ApiNetwork network;

    @Inject
    public AuthDataSource(ApiNetwork network){
        this.network=network;
    }

    public Observable<ResponseAuth> getToeknAuth(BodyAuthModel body) {
        return getObservable(network.getTokenAuth("application/json" ,"Basic cHJ1ZWJhc2RldjpwcnVlYmFzZGV2U2VjcmV0", body));
    }

    @NonNull
    public static Observable<ResponseAuth> getObservable(@NonNull final Call<ResponseAuth> responseAuth) {

        return Observable.create(new ObservableOnSubscribe<ResponseAuth>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseAuth> emitter) throws Exception {


                responseAuth.enqueue(new Callback<ResponseAuth>() {
                    @Override
                    public void onResponse(Call<ResponseAuth> call, Response<ResponseAuth> response) {

                        if(!emitter.isDisposed()){
                            emitter.onNext(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAuth> call, Throwable t) {

                        if(!emitter.isDisposed())
                            emitter.onError(t);


                    }
                });

            }
        });


    }

}
