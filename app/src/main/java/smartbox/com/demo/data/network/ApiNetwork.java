package smartbox.com.demo.data.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.data.network.model.ResponseMatch;
import smartbox.com.demo.presentation.model.BodyAuthModel;

public interface ApiNetwork {


    @POST("auth/users/login/anonymous")
    Call<ResponseAuth> getTokenAuth(@Header("Content-Type") String contentType, @Header("Authorization") String authorization,@Body BodyAuthModel body);

    //@POST("auth/users/login/anonymous")
    //Call<ResponseAuth> getTokenAuth(@Body BodyAuthModel body);

    @GET("sport/events")
    Call<ResponseMatch> getListMatch(@Header("Authorization") String credentials);

}
