package smartbox.com.demo.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseAuth {


    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("expiresIn")
        @Expose
        private String expiresIn;
        @SerializedName("tokenType")
        @Expose
        private String tokenType;
        @SerializedName("user")
        @Expose
        private User user;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getExpiresIn() {
            return expiresIn;
        }

        public void setExpiresIn(String expiresIn) {
            this.expiresIn = expiresIn;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

    }


    public class Profile {

        @SerializedName("language")
        @Expose
        private String language;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

    }

    public class Rbac {

        @SerializedName("role")
        @Expose
        private String role;
        @SerializedName("template")
        @Expose
        private String template;

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getTemplate() {
            return template;
        }

        public void setTemplate(String template) {
            this.template = template;
        }

    }


    public class User {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("rbac")
        @Expose
        private Rbac rbac;
        @SerializedName("profile")
        @Expose
        private Profile profile;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Rbac getRbac() {
            return rbac;
        }

        public void setRbac(Rbac rbac) {
            this.rbac = rbac;
        }

        public Profile getProfile() {
            return profile;
        }

        public void setProfile(Profile profile) {
            this.profile = profile;
        }

    }



}
