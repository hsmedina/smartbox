package smartbox.com.demo.presentation.presenter;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import smartbox.com.demo.data.network.model.ResponseMatch;
import smartbox.com.demo.data.preferences.AppPreferences;
import smartbox.com.demo.domain.interactor.DataInteractor;
import smartbox.com.demo.domain.interactor.DefaultObserver;
import smartbox.com.demo.presentation.view.contracts.MatchContract;
import smartbox.com.demo.presentation.view.util.DemoConstants;

public class MatchPresenter implements MatchContract.presenter {


    private MatchContract.view view;
    private final DataInteractor interactor;
    private final AppPreferences preferences;

    @Inject
    public MatchPresenter(DataInteractor interactor, AppPreferences preferences ){
        this.interactor= interactor;
        this.preferences=preferences;
    }

    @Override
    public void getListDataMatch() {
        showViewLoading();
        this.interactor.execute(new LocalObserver(), preferences.getValue(DemoConstants.DEMO_TOKEN));
    }

    @Override
    public void loadList(List<ResponseMatch.Item> items) {
        view.loadMatchData(items);
    }

    @Override
    public void setView(MatchContract.view view) {
        this.view=view;
    }

    @Override
    public void detachView() {
        this.view=null;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void showViewLoading() {
        this.view.showLoading();
    }

    private void hideViewLoading() {
        this.view.hideLoading();
    }

    private void showErrorMessage(String message) {
        this.view.showMessage(message);
    }


    private final class LocalObserver extends DefaultObserver<ResponseMatch> {


        @Override
        public void onNext(ResponseMatch responseMatch) {
            super.onNext(responseMatch);
            MatchPresenter.this.hideViewLoading();
            MatchPresenter.this.loadList(responseMatch.getData().getItems());
        }



        @Override
        public void onComplete() {
            super.onComplete();
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);

            Log.i("erro respuesta", exception.getMessage() + " ");

            MatchPresenter.this.hideViewLoading();
            MatchPresenter.this.showErrorMessage(exception.getLocalizedMessage());
        }
    }
}
