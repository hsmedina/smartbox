package smartbox.com.demo.presentation.view.util;

import android.Manifest;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import smartbox.com.demo.presentation.model.App;
import smartbox.com.demo.presentation.model.Device;
import smartbox.com.demo.presentation.model.Profile;


public class Util {

    public static String getVersionName(Context context){
        PackageManager manager = context.getPackageManager();
        try{
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        }catch(PackageManager.NameNotFoundException e){
            return "";
        }
    }


    public static String getCurrentLnaguague(){
        return Locale.getDefault().getDisplayLanguage();
    }

    public static App getAppModel(Context context){
        App app = new App();
        app.setVersion(Util.getVersionName(context));
        return app;
    }

    public static Profile getProfileModel(Context context){
        Profile profile = new Profile();
        profile.setLanguage(Util.getCurrentLnaguague());
        return profile;
    }

    public static DisplayMetrics dimensScreen(Context context){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getRealMetrics(displayMetrics);

        return displayMetrics;
    }

    public static String getModel(){
        return  Build.MANUFACTURER
                + " " + Build.MODEL;
                //+ " " + Build.VERSION.RELEASE
                //+ " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
    }

    public static Device getDeviceModel(Context context){
        Device device = new Device();
        device.setDeviceId(getPhoneId(context));
        device.setHeigth(String.valueOf(dimensScreen(context).heightPixels));
        device.setWidth(String.valueOf(dimensScreen(context).widthPixels));
        device.setModel(getModel());
        device.setName("myphone");
        device.setPlatform("android");
        device.setVersion(Build.VERSION.RELEASE);

        return device;
    }


    public static boolean checkAndRequestPermissions(AppCompatActivity activity) {
        int permissionSTATEPHONE = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_PHONE_STATE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionSTATEPHONE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity,listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }

        return true;
    }

    public static String getPhoneId(Context context){

        String id="";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);



        try{
            int permissionSTATEPHONE = ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_PHONE_STATE);

            if (permissionSTATEPHONE == PackageManager.PERMISSION_GRANTED)
                 id = telephonyManager.getDeviceId();

        }catch (NullPointerException e){

            id = "18252015";
        }


        return id;


    }


    public  static void showMessager(View view, String message){
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    public static Drawable getDrawableImage(Context context, int img){
        if(greaterLollipop())
            return context.getDrawable(img);
        else
            return context.getResources().getDrawable(img);
    }



    public static boolean greaterLollipop(){
        if (Build.VERSION.SDK_INT > 21) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(pDateString);
        return new Date().before(date);
    }

    public static boolean beforeToday(String target){

        Date tokenDateExp=null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);


        Date currentDate = null;
        Date dt = new Date();
        String currentDateTimeString = df.format(dt);

        try {

            tokenDateExp =  df.parse(target);
            currentDate = df.parse(currentDateTimeString);

            if(tokenDateExp.before(currentDate)){
                return true;
            }

        } catch (ParseException e) {
            return false;
        }

        return true;
    }





}
