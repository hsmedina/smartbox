package smartbox.com.demo.presentation.view.activitys;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smartbox.com.demo.R;
import smartbox.com.demo.di.components.DaggerLoginComponent;
import smartbox.com.demo.di.components.LoginComponent;
import smartbox.com.demo.di.modules.LoginModule;
import smartbox.com.demo.presentation.DemoApp;
import smartbox.com.demo.presentation.model.App;
import smartbox.com.demo.presentation.model.BodyAuthModel;
import smartbox.com.demo.presentation.model.Device;
import smartbox.com.demo.presentation.model.Profile;
import smartbox.com.demo.presentation.presenter.LoginPresenter;
import smartbox.com.demo.presentation.view.contracts.LoginContract;
import smartbox.com.demo.presentation.view.util.Util;

public class LoginActivity extends AppCompatActivity  implements LoginContract.view  {

    @BindView(R.id.user) EditText user;
    @BindView(R.id.pass) EditText pass;
    @BindView(R.id.toolbar)  Toolbar toolbar;
    @BindView(R.id.loader)  ProgressBar loader;
    @Inject LoginPresenter presenter;
    LoginComponent component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        initDagger();
        initToolBar();

        presenter.setView(this);
        Util.checkAndRequestPermissions(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.checkExpToken();
    }

    @Override
    protected void onResume() {
        super.onResume();

        user.setText(getString(R.string.defaultEmail));
        pass.setText(getString(R.string.defaultPassword));

    }

    @Override
    public void goToHome() {
        startActivity(new Intent(this,MatchsActivity.class));
    }

    @Override
    public void getAuth() {

        Gson gson = new Gson();
        Log.i("resp JSON", gson.toJson( buidlBody()));

        presenter.getToken(buidlBody());
    }


    @OnClick(R.id.btnlog) void doLogin(){
        getAuth();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage(String errorMessage) {

    }

    private void initDagger(){
        if (component == null) {
            component = DaggerLoginComponent.builder()
                    .appComponent(DemoApp.get(this).getAppComponent())
                    .loginModule(new LoginModule())
                    .build();
            component.inject(this);
        }
    }

    private BodyAuthModel buidlBody(){

        BodyAuthModel.User user = new BodyAuthModel.User();
        user.setProfile(Util.getProfileModel(this));

        BodyAuthModel body = new BodyAuthModel();
        body.setApp(Util.getAppModel(this));
        body.setDevice(Util.getDeviceModel(this));
        body.setUser(user);

        return body;
    }

    private void initToolBar(){
        toolbar.setBackground(Util.getDrawableImage(this, R.color.transparent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

    }

}
