package smartbox.com.demo.presentation.presenter;

import android.util.Log;

import javax.inject.Inject;

import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.data.preferences.AppPreferences;
import smartbox.com.demo.domain.interactor.DefaultObserver;
import smartbox.com.demo.domain.interactor.LoginInteractor;
import smartbox.com.demo.presentation.model.BodyAuthModel;
import smartbox.com.demo.presentation.view.contracts.LoginContract;
import smartbox.com.demo.presentation.view.util.DemoConstants;
import smartbox.com.demo.presentation.view.util.Util;

public class LoginPresenter implements LoginContract.presenter {


    private LoginContract.view view;
    private final LoginInteractor interactor;
    private final AppPreferences preferences;

    @Inject
    public LoginPresenter(LoginInteractor interactor,AppPreferences preferences ){
        this.interactor= interactor;
        this.preferences=preferences;
    }


    @Override
    public void checkExpToken() {
        if(Util.beforeToday(preferences.getValue(DemoConstants.EXP_TOKEN)))
            view.goToHome();

    }

    @Override
    public void getToken(BodyAuthModel auth) {
        showViewLoading();
        this.interactor.execute(new LocalObserver(), auth);
    }

    @Override
    public void setAuth(ResponseAuth responseAuth) {
        preferences.saveValue(DemoConstants.DEMO_TOKEN, responseAuth.getData().getAccessToken());
        preferences.saveValue(DemoConstants.EXP_TOKEN, responseAuth.getData().getExpiresIn());
        view.goToHome();
    }

    @Override
    public void setView(LoginContract.view view) {
        this.view=view;
    }

    @Override
    public void detachView() {
        this.view=null;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void showViewLoading() {
        this.view.showLoading();
    }

    private void hideViewLoading() {
        this.view.hideLoading();
    }

    private void showErrorMessage(String message) {
        this.view.showMessage(message);
    }


    private final class LocalObserver extends DefaultObserver<ResponseAuth> {


        @Override
        public void onNext(ResponseAuth responseAuth) {
            super.onNext(responseAuth);

            LoginPresenter.this.hideViewLoading();
            LoginPresenter.this.setAuth(responseAuth);
        }

        @Override
        public void onComplete() {
            super.onComplete();
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            Log.i("erro respuesta", exception.getMessage() + " ");
            LoginPresenter.this.hideViewLoading();
            LoginPresenter.this.showErrorMessage(exception.getLocalizedMessage());
        }
    }

}
