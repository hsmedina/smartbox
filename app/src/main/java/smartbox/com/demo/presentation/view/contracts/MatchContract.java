package smartbox.com.demo.presentation.view.contracts;

import java.util.List;

import smartbox.com.demo.data.network.model.ResponseMatch;
import smartbox.com.demo.presentation.base.BasePesenter;
import smartbox.com.demo.presentation.base.BaseView;

public interface MatchContract {
    interface view extends BaseView<presenter> {
        void loadMatchData(List<ResponseMatch.Item> items);
        void getListDataMatch();
    }

    interface presenter extends BasePesenter<view> {
        void getListDataMatch();
        void loadList(List<ResponseMatch.Item> items);
    }
}
