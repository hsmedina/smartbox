package smartbox.com.demo.presentation.view.activitys;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartbox.com.demo.R;
import smartbox.com.demo.data.network.model.ResponseMatch;
import smartbox.com.demo.di.components.DaggerMatchComponent;
import smartbox.com.demo.di.components.MatchComponent;
import smartbox.com.demo.di.modules.MatchModule;
import smartbox.com.demo.presentation.DemoApp;
import smartbox.com.demo.presentation.presenter.MatchPresenter;
import smartbox.com.demo.presentation.view.adapters.MatchAdapter;
import smartbox.com.demo.presentation.view.contracts.MatchContract;
import smartbox.com.demo.presentation.view.util.Util;

public class MatchsActivity extends AppCompatActivity implements MatchContract.view {


    @Inject
    MatchPresenter presenter;
    MatchComponent component;
    private MatchAdapter adapter;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.loader)
    ProgressBar loader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);
        ButterKnife.bind(this);

        initDagger();
        initToolBar();

        presenter.setView(this);

        Util.beforeToday("2017-08-26T23:27:26Z");

         /* try{
            if(Util.beforeToday("2017-08-26T23:27:26Z"))
                Toast.makeText(this, "is valid", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "is NOOT valid", Toast.LENGTH_SHORT).show();

        }catch (Exception e){
            Log.e("error date", e.getMessage());
        }*/


    }

    @Override
    public void loadMatchData(List<ResponseMatch.Item> items) {
        adapter = new MatchAdapter(this, items);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        getListDataMatch();
    }

    @Override
    public void getListDataMatch() {
        presenter.getListDataMatch();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage(String errorMessage) {
        Util.showMessager(rv, errorMessage);
    }

    private void initDagger(){
         if (component == null) {
            component = DaggerMatchComponent.builder()
                    .appComponent(DemoApp.get(this).getAppComponent())
                    .matchModule(new MatchModule())
                    .build();
            component.inject(this);
        }
    }

    private void initToolBar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.app_name));
    }
}
