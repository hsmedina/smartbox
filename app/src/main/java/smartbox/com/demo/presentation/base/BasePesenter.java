package smartbox.com.demo.presentation.base;

public interface BasePesenter<V> {
    void setView(V view);
    void detachView();
    void resume();
    void pause();
    void destroy();
}
