package smartbox.com.demo.presentation.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import smartbox.com.demo.R;
import smartbox.com.demo.data.network.model.ResponseMatch;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MatchHolder> {


    public Context context;
    List<ResponseMatch.Item> items;
   // Util helper;

    public MatchAdapter(Context context, List<ResponseMatch.Item> items){
        this.context=context;
        this.items=items;
    }

    @Override
    public MatchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_events, parent, false);
        MatchHolder vh = new MatchHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MatchHolder holder, int position) {
        ResponseMatch.Item item = (ResponseMatch.Item) items.get(position);

        if(position%2!=0)
            holder.border.setBackgroundColor(Color.GREEN);


        holder.local.setText(item.getHomeTeam().getName() + "   " + item.getHomeScore());
        holder.visit.setText(item.getAwayScore() + "   " + item.getAwayTeam().getName());
        holder.date.setText(formatDate(item.getStartDate()));
        holder.status.setText(item.getStatusCategory());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class MatchHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.border) LinearLayout border;
        @BindView(R.id.local) TextView local;
        @BindView(R.id.visit) TextView visit;
        @BindView(R.id.date_event) TextView date;
        @BindView(R.id.status_event) TextView status;


        public MatchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    private String formatDate(String dateMatch){
        final String OLD_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        final String NEW_FORMAT = "yyyy/MM/dd";


        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);

        try {
            Date d = sdf.parse(dateMatch);
            sdf.applyPattern(NEW_FORMAT);
            return sdf.format(d);

        } catch (ParseException e) {
            return dateMatch;
        }

    }

}
