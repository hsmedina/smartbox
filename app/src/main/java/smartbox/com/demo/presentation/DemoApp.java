package smartbox.com.demo.presentation;

import android.app.Application;
import android.content.Context;

import smartbox.com.demo.di.components.AppComponent;
import smartbox.com.demo.di.components.DaggerAppComponent;
import smartbox.com.demo.di.modules.AppModule;

public class DemoApp extends Application {

    public AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();


    }

    public static DemoApp get(Context context) {
        return (DemoApp) context.getApplicationContext();
    }


    public AppComponent getAppComponent(){
        return appComponent;
    }


}
