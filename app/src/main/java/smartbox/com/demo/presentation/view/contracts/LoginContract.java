package smartbox.com.demo.presentation.view.contracts;

import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.presentation.base.BasePesenter;
import smartbox.com.demo.presentation.base.BaseView;
import smartbox.com.demo.presentation.model.BodyAuthModel;

public interface LoginContract {
    interface view extends BaseView<presenter> {
        void goToHome();
        void getAuth();
    }

    interface presenter extends BasePesenter<view> {
        void checkExpToken();
        void getToken(BodyAuthModel auth);
        void setAuth(ResponseAuth responseAuth);
    }
}
