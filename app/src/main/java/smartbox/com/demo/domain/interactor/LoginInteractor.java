package smartbox.com.demo.domain.interactor;

import javax.inject.Inject;

import io.reactivex.Observable;
import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.domain.repository.LoginRepository;
import smartbox.com.demo.presentation.model.BodyAuthModel;

public class LoginInteractor extends UseCase<ResponseAuth,BodyAuthModel> {

    private final LoginRepository localRepository;

    @Inject
    public LoginInteractor(LoginRepository localRepository){
        this.localRepository=localRepository;
    }

    @Override
    Observable<ResponseAuth> buildUseCaseObservable(BodyAuthModel bodyAuthModel) {
        return localRepository.login(bodyAuthModel);
    }


}
