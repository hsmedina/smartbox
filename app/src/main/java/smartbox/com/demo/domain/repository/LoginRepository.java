package smartbox.com.demo.domain.repository;

import io.reactivex.Observable;
import smartbox.com.demo.data.network.model.ResponseAuth;
import smartbox.com.demo.presentation.model.BodyAuthModel;

public interface LoginRepository {
    Observable<ResponseAuth> login(BodyAuthModel body);
}
