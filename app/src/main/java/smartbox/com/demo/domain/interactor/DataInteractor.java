package smartbox.com.demo.domain.interactor;

import javax.inject.Inject;

import io.reactivex.Observable;
import smartbox.com.demo.data.network.model.ResponseMatch;
import smartbox.com.demo.domain.repository.DataRepository;


public class DataInteractor extends UseCase<ResponseMatch, String> {

    private final DataRepository dataRepository;

    @Inject
    public DataInteractor(DataRepository dataRepository){
        this.dataRepository=dataRepository;
    }

    @Override
    Observable<ResponseMatch> buildUseCaseObservable(String s) {
        return dataRepository.getList(s);
    }

}
