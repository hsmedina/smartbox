package smartbox.com.demo.domain.repository;

import io.reactivex.Observable;
import smartbox.com.demo.data.network.model.ResponseMatch;

public interface DataRepository {
    Observable<ResponseMatch> getList(String token);
}
